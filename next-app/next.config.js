// // /**
// //  * @type {import('next').NextConfig}
// //  */
// // const nextConfig = {
// //     /* config options here */
// //   }

// //   module.exports = nextConfig

// /**
//  * @type {import('next').NextConfig}
//  */
// const nextConfig = {
//     experimental: {
//       serverComponentsExternalPackages: [
//         "@react-email/components",
//         "@react-email/render",
//         "@react-email/tailwind",
//       ],
//     },
//   };

//   module.exports = nextConfig;

/** @type {import('next').NextConfig} */

const nextConfig = {  
  webpack: (
    config,
    { buildId, dev, isServer, defaultLoaders, nextRuntime, webpack }
  ) => {
    config.module.rules.push({
      test: /\.mjs$/,
      include: /node_modules/,
      type: "javascript/auto",
    });
    return config;
  },
  images: {
    remotePatterns: [
      {
        protocol: "https",
        hostname: "bit.ly",
      },
    ],
  },
};

module.exports = nextConfig;
