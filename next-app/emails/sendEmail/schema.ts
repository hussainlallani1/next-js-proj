import { z } from "zod";

export const schema = z.object({
  email: z.string().email().min(6),
  subject: z.string().min(8),
  text: z.string(),
  html: z.string().min(8),
});
