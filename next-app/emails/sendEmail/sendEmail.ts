"use server";
import nodemailer from "nodemailer";
import { schema } from "./schema";
import { ReactNode } from "react";

// EmailProvider Configuration Object
const emailProviderConfig = {
  server: {
    host: process.env.EMAIL_SERVER_HOST,
    port: Number(process.env.EMAIL_SERVER_PORT),
    secure: true,
    auth: {
      user: process.env.EMAIL_SERVER_USER,
      pass: process.env.EMAIL_SERVER_PASSWORD,
    },
  },
  from: process.env.EMAIL_FROM,
};

export const sendEmail = async (params: {
  email: string | undefined;
  subject: string;
  text: string;
  html: string;
}) => {
  const validation = schema.safeParse(params);

  if (validation.success) {
    try {
      const transport = nodemailer.createTransport(emailProviderConfig.server);

      // console.log("params.email: ", params.email);
      // console.log("params.subject: ", params.subject);
      // console.log("params.html: ", params.html);

      const sentEmailInfo = await transport.sendMail({
        from: emailProviderConfig.from,
        to: params.email,
        subject: params.subject,
        html: params.html,
        text: params.text,
      });

      // console.log("Verification email sent to:", "macpatel");
      console.log("sentEmailInfo.accepted:", sentEmailInfo.accepted);
      console.log("sentEmailInfo.envelope:", sentEmailInfo.envelope);
      console.log("sentEmailInfo.messageId:", sentEmailInfo.messageId);
      console.log("sentEmailInfo.pending:", sentEmailInfo.pending);
      console.log("sentEmailInfo.rejected:", sentEmailInfo.rejected);
      console.log("sentEmailInfo.response:", sentEmailInfo.response);

      return sentEmailInfo.messageId;
    } catch (error: any) {
      //   Log only the error message for other types of errors
      console.error("Error sending verification email:", error?.message);

      //   Return the error for further handling if needed
      return error?.message || error;
    }
  } else {
    console.log("Sendmail error: ", validation.error.errors[0].message);
    throw { error: validation.error.errors[0].message };
  }
};
