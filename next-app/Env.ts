class Env {
  //     # DATABASE_URL="mysql://laxonca2_next_app_admin786:admin@786@198.38.89.136:3306/laxonca2_next_app_db?connection_limit=1"
  // # SHADOW_DATABASE_URL="mysql://laxonca2_next_app_admin786:admin@786@198.38.89.136:3306/laxonca2_next_app_db1?connection_limit=1"

  // DATABASE_URL="mysql://root:root@localhost:3306/laxonca2_next_app_db"
  // SHADOW_DATABASE_URL="mysql://root:root@localhost:3306/laxonca2_next_app_db1"

  static NEXT_PUBLIC_CLOUDINARY_CLOUD_NAME: string =
    process.env.NEXT_PUBLIC_CLOUDINARY_CLOUD_NAME!;

  static NEXTAUTH_URL: string = process.env.NEXTAUTH_URL!;
  static NEXTAUTH_SECRET: string = process.env.NEXTAUTH_SECRET!;
  static NEXTAUTH_COOKIE_PREFIX: string = process.env.NEXTAUTH_COOKIE_PREFIX!;

  static GOOGLE_CLIENT_ID: string = process.env.GOOGLE_CLIENT_ID!;
  static GOOGLE_CLIENT_SECRET: string = process.env.GOOGLE_CLIENT_ID!;

  static EMAIL_SERVER_USER: string = process.env.EMAIL_SERVER_USER!;
  static EMAIL_SERVER_PASSWORD: string = process.env.EMAIL_SERVER_PASSWORD!;
  static EMAIL_SERVER_HOST: string = process.env.EMAIL_SERVER_HOST!;
  static EMAIL_SERVER_PORT: string = process.env.EMAIL_SERVER_PORT!;
  static EMAIL_FROM: string = process.env.EMAIL_FROM!;
}

export default Env;
