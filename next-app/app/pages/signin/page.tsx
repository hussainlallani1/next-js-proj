"use client";
import React, { useState } from "react";
import Login from "@/app/components/Login/Login";
import { useSearchParams } from "next/navigation";
import Modal from "@/app/components/Modal/Modal";

const SignInPage = () => {
  const searchParams = useSearchParams();
  const callbackUrl = searchParams.get("callbackUrl");
  const errorUrl = searchParams.get("error");
  const [modalOpen, setModalOpen] = useState(true);
  return (
    <div>
      <Modal open={modalOpen} onClose={() => setModalOpen(false)}>
        <Login callbackUrl={callbackUrl} errorUrl={errorUrl || undefined} />
      </Modal>
    </div>
  );
};

export default SignInPage;