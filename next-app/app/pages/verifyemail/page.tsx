"use client";
import { getProviders, signIn } from "next-auth/react";
import { useRouter } from "next/navigation";
import React from "react";

const VerifyEmailPage = async (user: any) => {
  const router = useRouter();
  // const searchParams = useSearchParams();
  // const email = searchParams.get("email");
  console.log("user inside callback: ", user.searchParams.email);
  // console.log("email: ", email);
  const providers = await getProviders();
  const handleResendVerification = async () => {
    // Trigger resend verification logic
    try {
      await signIn("email", {
        email: user.searchParams.email,
        // password: "password",
      });
      console.log("Sign in with email...");
      router.push("/api/auth/verify-request")
    } catch (error) {
      console.log("Callback signin with email error: ", error);
    }
  };
  return (
    <div>
      <p>Please verify your email address.</p>
      <button onClick={handleResendVerification}>
        Resend Verification Email to {user.searchParams.email}
      </button>
      {/* {Object.values(providers).map((provider) => (
        <div key={provider.name}>
          <button onClick={() => signIn(provider.id)}>
            Sign in with {provider.name}
            <br />
            Sign in with {provider.id}
          </button>
        </div>
      ))} */}
    </div>
  );
};

export default VerifyEmailPage;
