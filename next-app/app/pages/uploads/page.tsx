"use client";

import React, { useState } from "react";

const page = () => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const [file, setFile] = useState<File>();

  const onSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    if (!file) return 

    try {
        const data = new FormData()
        data.set('file',file)

        const res = await fetch('http://dev.laxon.ca/nextapp/upload.php', {
            method: 'POST',
            body: data
        })
        // handle the error
        if (!res.ok) throw new Error(await res.text())
        else alert("Uploaded")
    } catch {
        console.error(e)
    }
  }

  return (
    <div>
      <form onSubmit={onSubmit}>
        <input
          type="file"
          name="file"
          onChange={(e) => setFile(e.target.files?.[0])}
        />
        <input type="submit" value="Upload" />
      </form>
    </div>
  );
};

export default page;
