"use client"
import React from "react";

const EmailForm = (csrfToken: string | undefined) => {
  return (
    <div>
      <form
        action={`${process.env.NEXTAUTH_URL}/api/auth/signin/email`}
        method="POST"
      >
        <input
          type="hidden"
          name="csrfToken"
          // value="f2f68a55d323874eef904875492fe279b39e1528db40436f3c39c8482a26d05b"
          // value="bec46b08058a91d13e5582d786fd7dc9ddb5040aea3a6283b03ccf00daa7e6e9"
          value={`${csrfToken}`}
        />
        <input type="hidden" name="callbackUrl" value="/test" />
        <input type="hidden" name="source" value="testPage" />
        <label
          className="section-header"
          htmlFor="input-email-for-email-provider"
        >
          Email
        </label>
        <input
          id="input-email-for-email-provider"
          type="email"
          name="email"
          placeholder="email@example.com"
          value="test_macpatel40@gmail.com"
        />
        <button id="submitButton" type="submit">
          Sign in with Email
        </button>
      </form>
    </div>
  );
};

export default EmailForm;
