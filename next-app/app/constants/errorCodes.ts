export const VALIDATION_ERROR = "ValidationError";
export const VALIDATION_ERROR_MSG = "An unexpected error occurred.";

export const TOKEN_EXPIRED_OR_USED = "TokenExpiredOrUsed";
export const TOKEN_EXPIRED_OR_USED_MSG = "The token has been used or expired!";

export const INVALID_OR_EXPIRED_LINK = "InvalidOrExpiredToken";
export const INVALID_OR_EXPIRED_LINK_MSG = "Invalid or expired link!";

export const UNEXPECTED_ERROR = "UnexpectedError";
export const UNEXPECTED_ERROR_MSG = "An unexpected error occurred.";

export const PASSWORD_RESET_ERROR = "PasswordResetError";
export const PASSWORD_RESET_ERROR_MSG = "Error during password reset.";

export const PRISMA_CLIENT_VALIDATION_ERROR = "PrismaClientValidationError";
export const PRISMA_CLIENT_VALIDATION_ERROR_MSG =
  "An unexpected error occurred. Please try again or contact support. #prsm001";

export const PRISMA_CLIENT_INITIALIZATION_ERROR =
  "PrismaClientInitializationError";
export const PRISMA_CLIENT_INITIALIZATION_ERROR_MSG =
  "An unexpected error occurred. Please try again or contact support. #prsm001";

export const USER_NOT_LOCATED_OR_EXTERNAL_SIGN_UP =
  "UserNotLocatedOrExternalSignUp";
export const USER_NOT_LOCATED_OR_EXTERNAL_SIGN_UP_MSG =
  "User not located or signed up with an external login provider.";
