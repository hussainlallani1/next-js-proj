// pages/register.js
"use client";
import { useState } from "react";
import { useRouter } from "next/navigation";

const Register = () => {
  const router = useRouter();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleRegister = async (e) => {
    e.preventDefault();

    // Call the registration API route
    const response = await fetch("/api/register", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ email, password }),
    });

    if (response.ok) {
      // Registration successful, redirect to a confirmation page
      router.push("/register/confirmation");
    } else {
      // Handle registration error
      const data = await response.json();
      console.error("Registration error:", data.error);
    }
  };

  return (
    <div className="m-5 p-5 text-xl">
      <form onSubmit={handleRegister}>
        <div className="m-1 p-1">
          <label>
            Email:
            <input
              className="m-5 p-5"
              type="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              required
            />
          </label>
        </div>
        <div className="m-1 p-1">
          <label>
            Password:
            <input
              className="m-5 p-5"
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              required
            />
          </label>
        </div>
        <div className="m-5 p-5 ">
          <button type="submit" className="m-1 p-1 btn btn-primary">
            Register
          </button>
        </div>
      </form>
    </div>
  );
};

export default Register;
