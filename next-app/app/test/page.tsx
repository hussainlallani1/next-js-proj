"use client";
import { getCsrfToken, useSession } from "next-auth/react";
import { useRouter } from "next/navigation";
import { useEffect } from "react";
import Loading from "@/app/components/Loading";

const VerificationPage = () => {
  const route = useRouter();
  const { data: session } = useSession();
  useEffect(() => {
    const email = session?.user?.email;
    const fetchData = async () => {
      const csrfToken = await getCsrfToken();
      if (csrfToken && email) {
        // Redirect to the next route with parameters
        route.push(
          `/test/${encodeURIComponent(email)}/${encodeURIComponent(
            csrfToken
          )}`
        );
      }
    };
    fetchData();
  }, [session]);

  return (
    <div>
      <Loading />
    </div>
  );
};

export default VerificationPage;
