"use server";
import React, { ReactNode } from "react";
import Loading from "../components/Loading";

interface VerificationPageServerProps {
  csrfToken: string | null;
  children: ReactNode;
}

export const VerificationPageServer = ({
  csrfToken,
  children,
}: VerificationPageServerProps) => {
  if (csrfToken === null) {
    return <Loading />;
  }

  const csrfTokenString = String(csrfToken);

  return (
    <div>
      <br />
      csrfToken: {csrfTokenString}
      {/* Render other components */}
      {children}
    </div>
  );
};
