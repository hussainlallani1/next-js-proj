"use client";
import { useEffect, useState } from "react";
import { getCsrfToken } from "next-auth/react";

export const VerificationPageClient = () => {
  const [csrfToken, setCsrfToken] = useState<string | null>(null);

  useEffect(() => {
    const fetchData = async () => {
      const token = await getCsrfToken();
      setCsrfToken(typeof token === "string" ? token : null);
    };

    fetchData();
  }, []);

  return csrfToken;
};