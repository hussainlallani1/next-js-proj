import { randomBytes } from "crypto";
// import { PrismaClient } from "@prisma/client";
import prisma from "@/prisma/client";

// Use a named constant or configuration variable instead of magic numbers
const TOKEN_EXPIRATION_SECONDS = 86400;

export interface TokenOptions {
  expiresIn?: number; // Expiry time in seconds
}

export async function generateToken(
  identifier: string,
  options?: TokenOptions
): Promise<string> {
  const randomStr = randomBytes(32).toString("hex");
  const expires = new Date(
    Date.now() + (options?.expiresIn || TOKEN_EXPIRATION_SECONDS) * 1000
  );

  await prisma.verificationToken.create({
    data: {
      identifier,
      token: randomStr,
      expires,
    },
  });

  return randomStr;
}