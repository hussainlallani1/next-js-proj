// utils/send-verification-request.ts
import { createTransport, Transporter } from "nodemailer";

interface SendVerificationRequestParams {
  identifier: string;
  url: string;
  provider: {
    server: {
      host: string;
      port: number;
      auth: {
        user: string;
        pass: string;
      };
    };
    from: string;
  };
  theme: {
    brandColor?: string;
    buttonText?: string;
  };
}

async function sendVerificationRequest(
  params: SendVerificationRequestParams
): Promise<void> {
  const { identifier, url, provider, theme } = params;
  const { host } = new URL(url);

  // NOTE: You are not required to use `nodemailer`, use whatever you want.
  const transport: Transporter = createTransport(provider.server);

  const result = await transport.sendMail({
    to: identifier,
    from: provider.from,
    subject: `Sign in to ${host}`,
    text: text({ url, host }),
    html: html({ url, host, theme }),
  });

  const failed = result.rejected.concat(result.pending).filter(Boolean);
  if (failed.length) {
    throw new Error(`Email(s) (${failed.join(", ")}) could not be sent`);
  }
}

function html(params: {
  url: string;
  host: string;
  theme: { brandColor?: string; buttonText?: string };
}): string {
  // ... (provided HTML function)
  return ""; // Replace with your HTML implementation
}

function text(params: { url: string; host: string }): string {
  // ... (provided text function)
  return ""; // Replace with your text implementation
}

export default sendVerificationRequest;
