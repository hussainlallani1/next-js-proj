// dateUtils.ts
export const isDateExpired = (expirationDate: string): boolean => {
  const currentDate = new Date();
  const expiration = new Date(expirationDate);
  return expiration <= currentDate;
};
