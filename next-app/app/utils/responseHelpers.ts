// responseHelpers.ts
import { NextResponse } from 'next/server';

export const successResponse = (data: any) =>
  NextResponse.json({ data }, { status: 200 });

export const errorResponse = (code: string, message: string, details?: any) =>
  NextResponse.json({ error: { code, message, details } }, { status: 400 });
