import Link from "next/link";
import ProductCard from "./components/ProductCard";
import { getServerSession } from "next-auth";
import { authOptions } from "./api/auth/[...nextauth]/authOptions";
import HeavyComponent from "./components/HeavyComponent";
import SignIn from "./pages/custom-signin/page";
import AuthProvider from "./api/v1/auth/AuthProvider";
// import { useState } from "react";

export default async function Home() {
  // const [isVisible, setIsVisible] = useState(false)
  const session = await getServerSession(authOptions);
  return (
    <main className="relative h-screen">
      <h1>Hello, {session && <span>{session.user?.name}!</span>}</h1>
      <br></br>
      <Link href="/users">Users</Link>
      <br></br>
      <Link href="/upload">Upload</Link>
      <ProductCard />
      {/* <Image src={man} alt="man" /> */}
      <HeavyComponent />

      {/* <Image
        src="https://bit.ly/react-cover"
        alt="contain"
        fill
        className="object-cover"
        sizes="(max-width: 480px) 100vw, (max-width-768px) 50vw, 33vw"
        quality={100}
        priority */}
      {/* // sizes="100vw"
        // style={{ objectFit: "cover" }}
        // style={{ objectFit: "contain" }} */}
      {/* /> */}
    </main>
  );
}

// export async function generateMetadata(): Promise<Metadata> {
//   const product = await fetch("");

//   return {
//     title: "product.title",
//     description: "product.description",
//   };
// }
