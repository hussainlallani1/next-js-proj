import { z } from "zod";

export const schema = z.object({
  identifier: z.string().email().min(6),
  token: z
    .string()
    .refine((value) => /^[a-fA-F0-9]{64}$/.test(value), "Invalid token format"),
});
