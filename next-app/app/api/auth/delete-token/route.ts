"use server";
import prisma from "@/prisma/client";
import { NextRequest, NextResponse } from "next/server";
import * as errorCodes from "@/app/constants/errorCodes";
import { errorResponse, successResponse } from "@/app/utils/responseHelpers";
import { schema } from "./schema";

export async function POST(req: NextRequest) {
    const body = await req.json();
    const validation = schema.safeParse(body);
  
    if (!validation.success) {
      return errorResponse(
        errorCodes.VALIDATION_ERROR,
        errorCodes.VALIDATION_ERROR_MSG,
        validation.error.errors
      );
    }
  
    try {
      const data = await prisma.verificationToken.deleteMany({
        where: {
          identifier: body.identifier,
          token: body.token,
        },
      });
  
      if (data.count > 0) {
        return successResponse({
          identifier: body.identifier,
          token: body.token,
        });
      } else {
        return errorResponse(
          errorCodes.INVALID_OR_EXPIRED_LINK,
          errorCodes.INVALID_OR_EXPIRED_LINK_MSG
        );
      }
    } catch (error: any) {
      return errorResponse(
        errorCodes.UNEXPECTED_ERROR,
        errorCodes.UNEXPECTED_ERROR_MSG
      );
    } finally {
      await prisma.$disconnect();
    }
  }