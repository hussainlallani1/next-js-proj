import { z } from "zod";

export const schema = z.object({
  identifier: z.string().email().min(6),
  token: z.string(),
});
