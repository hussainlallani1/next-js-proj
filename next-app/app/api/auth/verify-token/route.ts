// "use server";
// import prisma from "@/prisma/client";
// import { NextRequest, NextResponse } from "next/server";
// import { z } from "zod";

// const schema = z.object({
//   identifier: z.string().email().min(6),
//   token: z.string(),
// });

// export async function POST(req: NextRequest) {
//   const body = await req.json();
//   const validation = schema.safeParse(body);

//   if (!validation.success) {
//     console.log(validation.error.errors);
//     return NextResponse.json(
//       {
//         error: {
//           code: "validation_error",
//           message: "Validation error",
//           details: validation.error.errors,
//         },
//       },
//       { status: 400 }
//     );
//   }

//   try {
//     const data = await prisma.verificationToken.findUnique({
//       where: {
//         identifier: body.identifier,
//         token: body.token,
//       },
//     });

//     if (data) {
//       const expirationDate = data?.expires!;
//       const currentDate = new Date();

//       if (expirationDate <= currentDate) {
//         return NextResponse.json(
//           { error: "The token has been used or expired!" },
//           { status: 500 }
//         );
//       }

//       return NextResponse.json(
//         {
//           // data: {
//           //   identifier: user?.identifier,
//           //   token: user?.token,
//           // },
//           data,
//         },
//         { status: 200 }
//       );
//     } else {
//       return NextResponse.json(
//         { error: "Invalid or expired reset link!" },
//         { status: 500 }
//       );
//     }
//   } catch (error: any) {
//     console.log("Code: ", Object.entries(error));
//     if (error.name === "PrismaClientValidationError") {
//       return NextResponse.json(
//         {
//           error:
//             "An unexpected error occurred. Please try again or contact support.",
//         },
//         { status: 500 }
//       );
//     } else {
//       return NextResponse.json(
//         { error: `Error during password reset: ${error?.message || error}` },
//         { status: 500 }
//       );
//     }
//   } finally {
//     await prisma.$disconnect();
//   }
// }

// export async function DELETE(req: NextRequest) {
//   const body = await req.json();
//   const validation = schema.safeParse(body);

//   if (!validation.success) {
//     return NextResponse.json(
//       { error: "Validation error", details: validation.error.errors },
//       { status: 400 }
//     );
//   }

//   try {
//     const data = await prisma.verificationToken.delete({
//       where: {
//         identifier: body.identifier,
//         token: body.token,
//       },
//     });

//     if (data) {
//       return NextResponse.json(
//         {
//           // data: {
//           //   identifier: user?.identifier,
//           //   token: user?.token,
//           // },
//           data,
//         },
//         { status: 200 }
//       );
//     } else {
//       return NextResponse.json(
//         {
//           error: {
//             code: "invalid_or_expired_link",
//             message: "The reset link is invalid or has expired.",
//           },
//         },
//         { status: 500 }
//       );
//     }
//   } catch (error: any) {
//     if (error.name === "PrismaClientValidationError") {
//       return NextResponse.json(
//         {
//           error:
//             "An unexpected error occurred. Please try again or contact support.",
//         },
//         { status: 500 }
//       );
//     } else {
//       return NextResponse.json(
//         { error: `Error during password reset: ${error?.message || error}` },
//         { status: 500 }
//       );
//     }
//   } finally {
//     await prisma.$disconnect();
//   }
// }

"use server";
import prisma from "@/prisma/client";
import { NextRequest } from "next/server";
import { successResponse, errorResponse } from "@/app/utils/responseHelpers";
import { isDateExpired } from "@/app/utils/isDateExpired";
import { z } from "zod";

const schema = z.object({
  identifier: z.string().email().min(6),
  token: z.string(),
});

export async function POST(req: NextRequest) {
  try {
    const body = await req.json();
    const { identifier, token } = schema.parse(body);
    // check token with identifier
    if (identifier && token) {

      const data = await prisma.verificationToken.findUnique({
        where: {
          identifier,
          token,
        },
      });

      if (data) {
        console.log("Data exists");
        // check token expiration
        const expirationDate = data.expires;
        console.log("Expiration date:", expirationDate);

        if (isDateExpired(expirationDate.toString())) {
          console.log("Token is expired");
          return errorResponse(
            "TOKEN_EXPIRED_OR_USED",
            "Invalid or expired link."
          );
        }

        // return token if not expired
        console.log("Token is not expired");
        return successResponse({
          identifier: data.identifier,
          token: data.token,
        });
      } else {
        // token is expired
        console.log("Data does not exist");
        return errorResponse(
          "INVALID_OR_EXPIRED_LINK",
          "Invalid or expired link."
        );
      }
    } else {
      // identifier or token is falsy
      console.log("Identifier or token is falsy");
      return errorResponse(
        "INVALID_IDENTIFIER_OR_TOKEN",
        "Invalid identifier or token."
      );
    }
  } catch (error: any) {
    // database or other error
    console.error("Error:", error);
    return errorResponse("UNEXPECTED_ERROR", "Unexpected error occurred.");
  } finally {
    // disconnect database
    await prisma.$disconnect();
  }
}

// export async function DELETE(req: NextRequest) {
//   const body = await req.json();
//   const validation = schema.safeParse(body);

//   if (!validation.success) {
//     return errorResponse(
//       errorCodes.VALIDATION_ERROR,
//       errorCodes.VALIDATION_ERROR_MSG,
//       validation.error.errors
//     );
//   }

//   try {
//     const data = await prisma.verificationToken.deleteMany({
//       where: {
//         identifier: body.identifier,
//         token: body.token,
//       },
//     });

//     if (data.count > 0) {
//       return successResponse({
//         identifier: body.identifier,
//         token: body.token,
//       });
//     } else {
//       return errorResponse(
//         errorCodes.INVALID_OR_EXPIRED_LINK,
//         errorCodes.INVALID_OR_EXPIRED_LINK_MSG
//       );
//     }
//   } catch (error: any) {
//     return errorResponse(
//       errorCodes.UNEXPECTED_ERROR,
//       errorCodes.UNEXPECTED_ERROR_MSG
//     );
//   } finally {
//     await prisma.$disconnect();
//   }
// }
