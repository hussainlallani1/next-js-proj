import { NextRequest } from "next/server";
import schema from "./schema";
import * as errorCodes from "@/app/constants/errorCodes";
import prisma from "@/prisma/client";
import { errorResponse, successResponse } from "@/app/utils/responseHelpers";
import { sendEmail } from "@/emails/sendEmail/sendEmail";
import { getVerifyEmailHtml } from "@/emails/templates/getVerifyEmailHtml";
import { generateToken } from "@/app/utils/tokenUtils";

export async function POST(req: NextRequest) {
  const body = await req.json();
  const email = body.email;
  const validation = schema.safeParse(body);

  if (!validation.success) {
    console.log("validation error: ", validation.error.errors);
    return errorResponse(
      errorCodes.VALIDATION_ERROR,
      errorCodes.VALIDATION_ERROR_MSG
    );
  }

  try {
    const newToken = await generateToken(email);
    const emailEncoded = encodeURIComponent(email);
    const newUrl = `${process.env.NEXTAUTH_URL}/auth/verify-request?identifier=${emailEncoded}&tk=${newToken}`;
    console.log("verifyEmailUrl: ",newUrl)
    const verificationEmailHtml = getVerifyEmailHtml(newUrl);
    const emailResult = await sendEmail({
      email: body.email,
      subject: "Verify your email",
      html: verificationEmailHtml,
      text: "<h1>Hello World!</h1>",
    });
    console.log("emailResult: ", emailResult);
    // const hashedPassword = await bcrypt.hash(body.newPassword, 10);

    // await prisma.user.update({
    //   where: { email: body.identifier },
    //   data: {
    //     hashedPassword,
    //   },
    // });

    return successResponse(body.identifier);
  } catch (error: any) {
    if (
      error.name === errorCodes.PRISMA_CLIENT_VALIDATION_ERROR ||
      errorCodes.PRISMA_CLIENT_INITIALIZATION_ERROR
    ) {
      return errorResponse(
        errorCodes.UNEXPECTED_ERROR,
        errorCodes.UNEXPECTED_ERROR_MSG
      );
    } else {
      return errorResponse(
        errorCodes.UNEXPECTED_ERROR,
        errorCodes.UNEXPECTED_ERROR_MSG
      );
    }
  } finally {
    await prisma.$disconnect();
  }
}
