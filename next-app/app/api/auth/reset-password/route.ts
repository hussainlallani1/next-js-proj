import { NextRequest } from "next/server";
import { schema } from "./schema";
import * as errorCodes from "@/app/constants/errorCodes";
import prisma from "@/prisma/client";
import bcrypt from "bcrypt";
import { errorResponse, successResponse } from "@/app/utils/responseHelpers";

export async function POST(req: NextRequest) {
  const body = await req.json();
  console.log("body: ", body);
  const validation = schema.safeParse(body);

  if (!validation.success) {
    console.log("validation error: ", validation.error.errors);
    return errorResponse(
      errorCodes.VALIDATION_ERROR,
      errorCodes.VALIDATION_ERROR_MSG
    );
  }

  try {
    const hashedPassword = await bcrypt.hash(body.newPassword, 10);

    await prisma.user.update({
      where: { email: body.identifier },
      data: {
        hashedPassword,
      },
    });

    return successResponse(body.identifier);
  } catch (error: any) {
    if (
      error.name === errorCodes.PRISMA_CLIENT_VALIDATION_ERROR ||
      errorCodes.PRISMA_CLIENT_INITIALIZATION_ERROR
    ) {
      return errorResponse(
        errorCodes.UNEXPECTED_ERROR,
        errorCodes.UNEXPECTED_ERROR_MSG
      );
    } else {
      return errorResponse(
        errorCodes.UNEXPECTED_ERROR,
        errorCodes.UNEXPECTED_ERROR_MSG
      );
    }
  } finally {
    await prisma.$disconnect();
  }
}
