import { z } from "zod";

const passwordSchema = z
  .string()
  .min(8, { message: "Password must be at least 8 characters long" })
  
// const passwordSchema = z
//   .string()
//   .min(8, { message: "Password must be at least 8 characters long" })
//   .max(100, { message: "Password cannot be longer than 100 characters" })
//   .refine(
//     (value) => /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d@$!%*?&]{8,}$/.test(value),
//     "Password must contain at least one letter, one number, and may include @$!%*?& characters"
//   );

export const schema = z.object({
  newPassword: passwordSchema,
  confirmNewPassword: passwordSchema,
  identifier: z.string().email().min(6),
  token: z.string().refine(
    (value) => /^[a-fA-F0-9]{64}$/.test(value),
    "Invalid token format"
  ),
}).refine(
  (data) => data.newPassword === data.confirmNewPassword,
  "Passwords don't match!"
);
