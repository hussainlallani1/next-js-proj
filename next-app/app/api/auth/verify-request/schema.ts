import { z } from "zod";

const schema = z.object({
  identifier: z.string().email(),
  token: z.string().refine((value) => /^[0-9a-fA-F]{64}$/.test(value), {
    message: "Invalid token format",
  }),
});

export default schema;
