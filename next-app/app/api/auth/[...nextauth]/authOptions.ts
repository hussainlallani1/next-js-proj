import { NextAuthOptions } from "next-auth";
import GoogleProvider from "next-auth/providers/google";
import EmailProvider from "next-auth/providers/email";
import CredentialsProvider from "next-auth/providers/credentials";
import { PrismaAdapter } from "@next-auth/prisma-adapter";
import { PrismaClient, User } from "@prisma/client";
import bcrypt from "bcryptjs";
import nodemailer from "nodemailer";
import { generateToken } from "@/app/utils/tokenUtils";

// EmailProvider Configuration Object
const emailProviderConfig = {
  server: {
    host: process.env.EMAIL_SERVER_HOST,
    port: Number(process.env.EMAIL_SERVER_PORT),
    auth: {
      user: process.env.EMAIL_SERVER_USER,
      pass: process.env.EMAIL_SERVER_PASSWORD,
    },
  },
  from: process.env.EMAIL_FROM,
};

const prisma = new PrismaClient();

// Store fetched User from database
let userDb: User | null = null;

export const authOptions: NextAuthOptions = {
  adapter: PrismaAdapter(prisma),
  // Configure one or more authentication providers
  providers: [
    CredentialsProvider({
      name: "Credentials",
      credentials: {
        email: { label: "Email", type: "email", placeholder: "Email" },
        password: {
          label: "Password",
          type: "password",
          placeholder: "Password",
        },
      },
      async authorize(credentials, req) {
        if (!credentials?.email || !credentials.password) return null;
        try {
          userDb = await prisma.user.findUnique({
            where: { email: credentials?.email },
          });

          if (!userDb) return null;
          const passwordsMatch = await bcrypt.compare(
            credentials.password,
            userDb!.hashedPassword!
          );

          return passwordsMatch ? userDb : null;
        } catch (error) {
          console.error("Error during sign-in:", error);
          // Handle errors or redirect to an error page
          return "/error";
        } finally {
          // Close the Prisma client connection
          await prisma.$disconnect();
        }
      },
    }),
    // EmailProvider({
    //   ...emailProviderConfig,
    //   sendVerificationRequest({
    //     identifier: email,
    //     url,
    //     expires,
    //     provider,
    //     token,
    //     theme,
    //   }): void {
    //     // console.log(
    //     //   "identifier: ",
    //     //   email,
    //     //   "url: ",
    //     //   url,
    //     //   "expires: ",
    //     //   expires,
    //     //   "provider: ",
    //     //   provider,
    //     //   "token: ",
    //     //   token,
    //     //   "theme: ",
    //     //   theme
    //     // );

    //     console.log("userDb: ", userDb);
    //     console.log("email: ", email);

    //     // Check if the email starts with the test prefix
    //     const pendingVerificationStr = "pending-verification_";
    //     const resetPasswordStr = "reset-password_";

    //     const isVerifyEmail = email.startsWith(pendingVerificationStr);
    //     const isResetPasswordEmail = email.startsWith(resetPasswordStr);

    //     // Remove any test prefix from the email
    //     const cleanedVerifyEmail = email.replace("pending-verification_", "");
    //     const cleanedResetPasswordEmail = email.replace("reset-password_", "");

    //     if (isVerifyEmail) console.log("It's from verify page!");
    //     if (isResetPasswordEmail) console.log("It's from reset page!");

    //     try {
    //       const transport = nodemailer.createTransport({
    //         host: emailProviderConfig.server.host,
    //         port: emailProviderConfig.server.port,
    //         secure: true,
    //         auth: emailProviderConfig.server.auth,
    //       });

    //       transport.sendMail({
    //         from: emailProviderConfig.from,
    //         to: cleanedResetPasswordEmail,
    //         subject: "Verify your email address",
    //         html: `<p>Click <a href="${url}">here</a> to verify your email address.</p>`,
    //       });
    //       console.log("Verification email sent to:", cleanedResetPasswordEmail);
    //       console.log("Verification URL:", url);
    //     } catch (error) {
    //       console.error("Error sending verification email:", error);
    //     }
    //   },
    // }),
    GoogleProvider({
      // ! at the end means mandatory
      clientId: process.env.GOOGLE_CLIENT_ID!,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET!,
    }),
    // ...add more providers here
  ],
  pages: {
    // signIn: "/signin", // Change the path to your desired custom sign-in page
    // signIn: "/signin",
    // signOut: "/signout",
    // error: "/auth/error", // Error code passed in query string as ?error=
    // verifyRequest: "/auth/verify-request", // (used for check email message)
    // newUser: "/auth/new-user", // New users will be directed here on first sign in (leave the property out if not of interest)
  },
  callbacks: {
    async signIn({ user, account, profile, email, credentials }) {
      try {
        // Retrieve user information from the database
        const userDb = await prisma.user.findUnique({
          where: { email: credentials?.email.toString() },
        });

        if (userDb && !userDb.emailVerified) {
          // Check if the email is verified and sign-in is with credentials

        try {
            // const newToken = await generateToken(userDb?.email!);
            // Redirect logic here, including user and CSRF token
            const emailDecoded = encodeURIComponent(user.email!).toString();
            const redirectPath = `/auth/pending-verification?identifier=${emailDecoded}`;
            // const redirectPath = `/auth/pending-verification?identifier=${emailDecoded}&tk=${newToken}`;
            return redirectPath;
          } catch (error) {
            console.log("Error retrieving the token: ", error);
            return "/auth/error";
          }
        }
        // Proceed with the sign-in
        return true;
      } catch (error) {
        console.error("Error getting userDb:", error);
        // Handle errors or redirect to an error page
        return "/auth/error";
      } finally {
        // Close the Prisma client connection
        await prisma.$disconnect();
      }
    },
    async jwt({ token, trigger, session, user, account, profile, isNewUser }) {
      // Persist the OAuth access_token to the token right after signin
      if (account) {
        token.accessToken = account.access_token;
      }
      return token;
    },
  },
  session: {
    strategy: "jwt",
    // strategy: "database",
  },
  theme: {
    colorScheme: "light", // "auto" | "dark" | "light"
    brandColor: "", // Hex color code
    logo: "", // Absolute URL to image
    buttonText: "", // Hex color code
  },
  logger: {
    error(code, metadata) {
      console.error(code, metadata);
    },
    warn(code) {
      console.warn(code);
    },
    debug(code, metadata) {
      console.debug(code, metadata);
    },
  },
};
