"use server";
import prisma from "@/prisma/client";
import { NextRequest, NextResponse } from "next/server";
import { z } from "zod";
import * as errorCodes from "@/app/constants/errorCodes";
import { errorResponse, successResponse } from "@/app/utils/responseHelpers";
import { generateToken } from "@/app/utils/tokenUtils";

const schema = z.object({
  email: z.string().email().min(6),
});

export async function POST(req: NextRequest) {
  const body = await req.json();
  const validation = schema.safeParse(body);

  if (!validation.success) {
    return errorResponse(errorCodes.VALIDATION_ERROR,validation.error.errors[0].message)
    // return NextResponse.json(
    //   {
    //     error: errorCodes.VALIDATION_ERROR_MSG,
    //     details: validation.error.errors,
    //   },
    //   { status: 400 }
    // );
  }

  try {
    const user = await prisma.user.findUnique({ where: { email: body.email } });

    if (!user?.hashedPassword) {
      return errorResponse(
        errorCodes.USER_NOT_LOCATED_OR_EXTERNAL_SIGN_UP,
        errorCodes.USER_NOT_LOCATED_OR_EXTERNAL_SIGN_UP_MSG
      );
    }

    const newToken = await generateToken(user?.email!);

    return successResponse({
      identifier: user?.email!,
      token: newToken,
      url: process.env.NEXTAUTH_URL,
    });
  } catch (error: any) {
    if (
      error.name === errorCodes.PRISMA_CLIENT_VALIDATION_ERROR ||
      errorCodes.PRISMA_CLIENT_INITIALIZATION_ERROR
    ) {
      return errorResponse(
        errorCodes.PRISMA_CLIENT_INITIALIZATION_ERROR,
        errorCodes.PRISMA_CLIENT_INITIALIZATION_ERROR_MSG
      );
    } else {
      return NextResponse.json(
        { error: `Error during password reset: ${error?.message || error}` },
        { status: 500 }
      );
    }
  } finally {
    await prisma.$disconnect();
  }
}
