import React from "react";
import SignIn from "../../../pages/custom-signin/page.jsx";

const AuthPage = () => {
  return (
    <div>
      <SignIn />
    </div>
  );
};

export default AuthPage;
