import { render } from "jsx-email";
import nodemailer from "nodemailer";
import { z } from "zod";
import { BatmanTemplate } from "@/email/BatmanEmail";
import { NextRequest, NextResponse } from "next/server";

const schema = z.object({
  identifier: z.string().email(),
});

export async function POST(request: NextRequest) {
  
  const body = await request.json() || {};

  const validation = schema.safeParse(body);

  if (!validation.success)
    return NextResponse.json(validation.error.errors[0].message, {
      status: 400,
    });

  try {
    console.log("Request Body: ", body.identifier);

    const html = await render(
      <BatmanTemplate email="macpatel40@gmail.com" name="Batman" />
    );

    const transport = nodemailer.createTransport({
      host: "mocha3037.mochahost.com",
      port: 465,
      secure: true,
      auth: {
        user: "admin@laxon.ca",

        pass: "admin@7860",
      },
    });

    const info = await transport.sendMail({
      from: "admin@laxon.ca",
      to: "macpatel40@gmail.com",
      subject: "Batman Template",
      text: "Hello world?", // plain text body
      html: html,
    });

    console.log("Message sent: %s", info.messageId);
    return NextResponse.json({ msg: "Email Sent!" }, { status: 200 });
  } catch (error) {
    console.error("Error sending email:", error);
    return NextResponse.json(
      { error: `Error sending email: ${error}` },
      { status: 400 }
    );
  }
}
