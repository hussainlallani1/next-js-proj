import { render } from "jsx-email";
import nodemailer from "nodemailer";

import { BatmanTemplate } from "@/email/BatmanEmail";
import { NextRequest, NextResponse } from "next/server";

export async function GET(request: NextRequest) {
  try {
    const html = await render(
      <BatmanTemplate email="macpatel40@gmail.com" name="Batman" />
    );

    const transport = nodemailer.createTransport({
      host: process.env.EMAIL_SERVER_HOST,
      port: Number(process.env.EMAIL_SERVER_PORT),
      secure: true,
      auth: {
        user: process.env.EMAIL_SERVER_USER,
        pass: process.env.EMAIL_SERVER_PASSWORD,
      },
    });

    const info = await transport.sendMail({
      from: "admin@laxon.ca",
      to: "macpatel40@gmail.com",
      subject: "Batman Template",
      text: "Hello world?", // plain text body
      html: html,
    });

    console.log("Message sent: %s", info.messageId);
    return NextResponse.json({ msg: "Email Sent!" }, { status: 200 });
  } catch (error) {
    console.error("Error sending email:", error);
    return NextResponse.json(
      { error: "Error sending email:" },
      { status: 400 }
    );
  }
}
