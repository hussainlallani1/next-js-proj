"use client";
// Importing necessary modules
import React, { FormEvent, useEffect, useState } from "react";
import { useRouter, useSearchParams, usePathname } from "next/navigation";
import { z } from "zod";
import Loading from "@/app/components/Loading";
import * as errorCodes from "@/app/constants/errorCodes";

// Function to request token verification
const requestTokenVerification = async ({
  identifier,
  token,
}: {
  identifier: string | undefined;
  token: string | undefined;
}) => {
  try {
    const response = await fetch("/api/auth/verify-token", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ identifier, token }),
    });

    if (response.ok) {
      const result = await response.json();
      return result;
    } else {
      const errorData = await response.json();
      console.log("errorData: ", errorData);
      throw errorData;
    }
  } catch (error) {
    throw {
      code: error?.error?.code,
      message: error?.error?.message,
    };
  }
};

// Main component
const VerifyEmailPage = () => {
  // Extracting parameters and initializing variables
  const params = useSearchParams();
  const identifier = params.get("identifier")?.toString();
  const token = params.get("tk")?.toString();
  const router = useRouter();
  const pathname = usePathname();
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState({});
  // useEffect for initial data fetching
  useEffect(() => {
    const fetchData = async () => {
      try {
        const { data: tokenVerificationResult } =
          await requestTokenVerification({
            identifier,
            token,
          });

        console.log("tokenVerificationResult: ", tokenVerificationResult);

        if (tokenVerificationResult) {
          if (
            tokenVerificationResult.identifier === identifier &&
            tokenVerificationResult.token === token
          ) {
            try {
              const verificationRequestResponse = await fetch(
                "/api/auth/verify-request",
                {
                  method: "POST",
                  headers: {
                    "Content-Type": "application/json",
                  },
                  body: JSON.stringify({ identifier, token }),
                }
              );

              if (verificationRequestResponse.ok) {
                const result = await verificationRequestResponse.json();
                deleteToken({ identifier, token });
                return result;
              } else {
                const errorData = await verificationRequestResponse.json();
                throw errorData;
              }
            } catch (error) {
              throw {
                code: error?.error?.code,
                message: error?.error?.message,
              };
            }

            // const currentUrl = window.location.href.split("?")[0];
            // window.history.replaceState(null, "", currentUrl);
            setLoading(false);
          }
        }
      } catch (error) {
        console.error("Error during token verification:", error);
        setError({ error });
        setLoading(false);
      }
    };

    if (identifier && token) {
      console.log("fetchData()");
      fetchData();
    } else {
      setError({
        error: {
          code: errorCodes.INVALID_OR_EXPIRED_LINK,
          message: errorCodes.INVALID_OR_EXPIRED_LINK_MSG,
        },
      });
      setLoading(false);
    }
  }, []);

  const deleteToken = async ({
    identifier,
    token,
  }: {
    identifier: string | undefined;
    token: string | undefined;
  }) => {
    try {
      setError({});
      const deleteTokenResponse = await fetch("/api/auth/delete-token", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          identifier,
          token,
        }),
      });
      if (deleteTokenResponse.ok) {
        // Password reset and token deletion were successful
        const deleteTokenResult = await deleteTokenResponse.json();
        console.log("deleteTokenResult: ", deleteTokenResult);

        // Append "/success" to the current pathname
        const newPathname = `${pathname}/success`;
        router.push(newPathname);
        return deleteTokenResult;
      } else {
        // Handle errors related to token deletion
        const deleteTokenError = await deleteTokenResponse.json();
        console.error("Error deleting token:", deleteTokenError);
        console.log("deleteTokenError: ", deleteTokenError);
        return deleteTokenError;
      }
    } catch (error) {
      console.log("Error fetching: /api/auth/delete-token ", error);
    } finally {
      setLoading(false);
    }
  };

  // JSX for error message
  const errorMessageHtml = (
    <div className="pt-5 mt-5 w-full text-secondary text-2xl">
      {error?.error?.message ?? ""}
    </div>
  );

  // JSX for form
  const resetFormHtml = (
    <>
      <br />
      <div>
        <h1 className="text-secondary">{identifier}</h1>
      </div>
      <div>
        <p className="mt-6 text-base leading-7 text-gray-600 text-xl">
          has been succesfully verified!
        </p>
      </div>
      <div className="mt-10 flex items-center justify-center gap-x-6"></div>
    </>
  );

  // JSX for the main component
  return (
    <>
      {loading ? (
        <Loading />
      ) : (
        <main className="grid min-h-full place-items-center bg-white px-6 py-24 sm:py-32 lg:px-8">
          <div className="max-w-96 text-center word-wrap">
            <h1 className="mt-4 text-3xl font-bold tracking-tight text-gray-900 sm:text-5xl">
              Email Verification
            </h1>

            {error?.error
              ? error.error && error.error.code === errorCodes.VALIDATION_ERROR
                ? resetFormHtml
                : errorMessageHtml
              : resetFormHtml}
          </div>
        </main>
      )}
    </>
  );
};

export default VerifyEmailPage;
