"use client";
import React, { FormEvent, ReactNode, useState } from "react";
import { useRouter } from "next/navigation";
import { useFormStatus } from "react-dom";
import { z } from "zod";
import { sendEmail } from "@/emails/sendEmail/sendEmail";
import { generateResetPasswordHtml } from "@/emails/generateResetPasswordHtml";

const schema = z.object({
  email: z.string().email().min(6),
});

// Service for handling API requests
const requestPasswordReset = async (email: string) => {
  try {
    const response = await fetch("/api/auth/forgot-password", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        // "X-CSRF-Token": csrfToken || "", // Include the CSRF token in the headers
      },
      body: JSON.stringify({ email }),
      credentials: "same-origin",
    });

    if (response.ok) {
      const result = await response.json();
      console.log("result: ", result);
      return result;
    } else {
      const errorData = await response.json();
      console.log("errorData.error: ", errorData.error);
      throw errorData.error.message;
    }
  } catch (error) {
    console.error("Response Error: ", error);
    throw error;
  }
};

const ForgotPasswordPage = () => {
  const router = useRouter();
  const formStatus = useFormStatus();
  const [successMessage, setSuccessMessage] = useState<ReactNode>();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState({});

  const successMessageHtml = (
    <h1 className="text-secondary text-lg m-5 p-5">{successMessage}</h1>
  );
  const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
    setError({});
    event.preventDefault();

    try {
      setLoading(true);

      const formData = new FormData(event.currentTarget);
      const email = formData.get("email")?.toString();
      const validation = schema.safeParse({ email });

      if (validation.success) {
        // const { identifier, token, url } = await requestPasswordReset(email!);
        const { data } = await requestPasswordReset(email!);
        const { identifier, token, url } = data;
        const emailEncoded = encodeURIComponent(identifier!);

        const resetUrl = `${url}/auth/reset-password?identifier=${emailEncoded}&token=${token}`;
        const emailContent = generateResetPasswordHtml(resetUrl);

        const emailResult = await sendEmail({
          email,
          subject: "Verify your email",
          html: emailContent,
          text: emailContent,
        });
        console.log("emailResult: ", emailResult);
        console.log(
          "URL: ",
          `${url}/auth/reset-password?identifier=${emailEncoded}&token=${token}`
        );
        setSuccessMessage(
          <>
            <p className="mt-6 text-base leading-7 text-gray-600">
              Check your inbox for the password reset link – it's on its way.{" "}
            </p>
            <br />
            <br />
            <p className="text-md text-black">{email}</p>
          </>
        );
        // router.push(`/auth/forgot-password/success/${emailEncoded}`);
      } else {
        setError({ error: validation.error.errors[0].message });
      }
    } catch (error) {
      // Handle error
      setError({ error });
    } finally {
      setLoading(false);
    }
  };

  return (
    <main className="grid min-h-full place-items-center bg-white px-6 py-24 sm:py-32 lg:px-8">
      <div className="max-w-96 text-center word-wrap">
        <h1 className="mt-4 text-3xl font-bold tracking-tight text-gray-900 sm:text-5xl">
          Forgot Password?
        </h1>
        {successMessage ? (
          successMessageHtml
        ) : (
          <>
            <p className="mt-6 text-base leading-7 text-gray-600">
              Lost access to your account? Don't worry, we'll help you securely
              reset your password.
            </p>
            <div className="mt-10 flex items-center justify-center gap-x-6">
              <form onSubmit={handleSubmit}>
                <p className="font-medium text-slate-700 pb-2">Email Address</p>
                <input
                  id="input-email-for-email-provider"
                  type="email"
                  name="email"
                  defaultValue=""
                  className="w-full py-3 w- border border-slate-200 rounded-lg px-3 focus:outline-none focus:border-slate-500 hover:shadow"
                  placeholder="Enter email address"
                />
                <br />
                <article className="w-64 text-wrap">
                  <p className="text text-red-400">
                    {error ? error.error : ""}
                  </p>
                </article>
                <br />
                <button
                  className="w-full py-3 font-medium text-white bg-indigo-600 hover:bg-indigo-500 rounded-lg border-indigo-500 hover:shadow inline-flex space-x-2 items-center justify-center"
                  type="submit"
                  disabled={loading}
                >
                  {loading && (
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth="1.5"
                      stroke="currentColor"
                      className="w-6 h-6 animate-spin"
                    >
                      <circle
                        cx="12"
                        cy="12"
                        r="10"
                        stroke="currentColor"
                        strokeWidth="4"
                      />
                    </svg>
                  )}

                  <span>Reset Password</span>
                </button>
                <p className="text-center">
                  Not registered yet?{" "}
                  <a
                    href="/register"
                    className="text-indigo-600 font-medium inline-flex space-x-1 items-center"
                  >
                    <span>
                      {formStatus?.pending ? "Loading..." : "Register Now"}{" "}
                    </span>
                    <span>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="h-4 w-4"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                        strokeWidth="2"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14"
                        />
                      </svg>
                    </span>
                  </a>
                </p>
                <br />
              </form>
            </div>
          </>
        )}
      </div>
    </main>
  );
};

export default ForgotPasswordPage;
