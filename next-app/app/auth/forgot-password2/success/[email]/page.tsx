import React from "react";
import { z } from "zod";

const schema = z.object({
  email: z.string().email(),
});

const SuccessPage = ({ params: { email } }: { params: { email: string } }) => {
  const emailDecoded = decodeURIComponent(email);

  return (
    <div>
      <main className="grid min-h-full place-items-center bg-white px-6 py-24 sm:py-32 lg:px-8">
        <div className="text-center">
          <h1 className="mt-4 text-secondary text-3xl font-bold tracking-tight text-gray-900 sm:text-5xl">
            Reset password
          </h1>
          <p className="mt-6 text-base leading-7 text-gray-600">
            We have emailed your password reset link.
          </p>
          <p className="mt-4 text-xl font-bold tracking-tight text-gray-600 sm:text-2xl">
            {emailDecoded}
          </p>
          <div className="mt-10 flex items-center justify-center gap-x-6">
            <p className="w-full">
              Password reset is only for accounts registered directly with us.{" "}
              <br />
              For external logins (e.g., Gmail), use your provider's recovery
              options.
            </p>
          </div>
        </div>
      </main>
    </div>
  );
};

export default SuccessPage;
