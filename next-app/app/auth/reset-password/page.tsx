"use client";
// Importing necessary modules
import React, { FormEvent, useEffect, useState } from "react";
import { useRouter, useSearchParams, usePathname } from "next/navigation";
import { z } from "zod";
import Loading from "@/app/components/Loading";
import * as errorCodes from "@/app/constants/errorCodes";

// Validation schema
const schema = z
  .object({
    newPassword: z.string().min(8),
    confirmNewPassword: z.string().min(8),
  })
  .refine((data) => data.newPassword === data.confirmNewPassword, {
    message: "Passwords don't match!",
  });

// Function to request token verification
const requestTokenVerification = async ({
  identifier,
  token,
}: {
  identifier: string | undefined;
  token: string | undefined;
}) => {
  try {
    const response = await fetch("/api/auth/verify-token", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ identifier, token }),
    });

    if (response.ok) {
      const result = await response.json();
      return result;
    } else {
      const errorData = await response.json();
      throw errorData;
    }
  } catch (error) {
    throw {
      code: error?.error?.code,
      message: error?.error?.message,
    };
  }
};

// Main component
const ResetPasswordPage = () => {
  // Extracting parameters and initializing variables
  const params = useSearchParams();
  const identifier = params.get("identifier")?.toString();
  const token = params.get("token")?.toString();
  const router = useRouter();
  const pathname = usePathname();
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState({});
  // useEffect for initial data fetching
  useEffect(() => {
    const fetchData = async () => {
      try {
        const { data: verificationResult } = await requestTokenVerification({
          identifier,
          token,
        });

        if (verificationResult) {
          if (
            verificationResult.identifier === identifier &&
            verificationResult.token === token
          ) {
            const currentUrl = window.location.href.split("?")[0];
            window.history.replaceState(null, "", currentUrl);
            setLoading(false);
          }
        }
      } catch (error) {
        setError({ error });
        console.error("Error during token verification:", error);
        setLoading(false);
      }
    };

    if (identifier && token) {
      console.log("fetchData()");
      fetchData();
    } else {
      setError({
        error: {
          code: errorCodes.INVALID_OR_EXPIRED_LINK,
          message: errorCodes.INVALID_OR_EXPIRED_LINK_MSG,
        },
      });
      setLoading(false);
    }
  }, []);

  // Handling form submission
  const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    try {
      setLoading(true);

      const formData = new FormData(event.currentTarget);
      const newPassword = formData.get("newPassword")?.toString();
      const confirmNewPassword = formData.get("confirmNewPassword")?.toString();

      const validation = schema.safeParse({
        newPassword,
        confirmNewPassword,
      });

      if (validation.success) {
        setError({});
        try {
          const resetPasswordResponse = await fetch(
            "/api/auth/reset-password",
            {
              method: "POST",
              headers: {
                "Content-Type": "application/json",
              },
              body: JSON.stringify({
                identifier,
                token,
                newPassword,
                confirmNewPassword,
              }),
            }
          );

          if (!resetPasswordResponse.ok) {
            const errorData = await resetPasswordResponse.json();
            throw errorData;
          } else {
            try {
              const deleteTokenResponse = await fetch(
                "/api/auth/delete-token",
                {
                  method: "POST",
                  headers: {
                    "Content-Type": "application/json",
                  },
                  body: JSON.stringify({
                    identifier,
                    token,
                  }),
                }
              );
              if (deleteTokenResponse.ok) {
                // Password reset and token deletion were successful
                const deleteTokenResult = await deleteTokenResponse.json();
                console.log("deleteTokenResult: ", deleteTokenResult);

                // Append "/success" to the current pathname
                const newPathname = `${pathname}/success`;
                router.push(newPathname);
                return deleteTokenResult;
              } else {
                // Handle errors related to token deletion
                const deleteTokenError = await deleteTokenResponse.json();
                console.error("Error deleting token:", deleteTokenError);
                console.log("deleteTokenError: ", deleteTokenError);
                return deleteTokenError;
              }
            } catch (error) {
              console.log("Error fetching: /api/auth/delete-token ", error);
            }
          }
        } catch (error) {
          // console.error("Error during password reset:", error);
          // throw {
          //   code: error?.error?.code,
          //   message: error?.error?.message,
          // };
          setError({
            error: {
              code: error?.error?.code,
              message: error?.error?.message,
            },
          });
        }
      } else {
        setError({
          error: {
            code: errorCodes.VALIDATION_ERROR,
            message: validation.error.errors[0]?.message,
          },
        });
      }
    } catch (error) {
      setError({
        error: {
          code: errorCodes.UNEXPECTED_ERROR,
          message: errorCodes.UNEXPECTED_ERROR_MSG,
          details: error,
        },
      });
    } finally {
      setLoading(false);
    }
  };

  // JSX for error message
  const errorMessageHtml = (
    <div className="pt-5 mt-5 w-full text-secondary text-2xl">
      {error?.error?.message ?? ""}
    </div>
  );

  // JSX for form
  const resetFormHtml = (
    <>
      <p className="mt-6 text-base leading-7 text-gray-600">
        To enhance the security of your account, please choose a new password
        and confirm it below.
      </p>

      <div className="mt-10 flex items-center justify-center gap-x-6">
        <form onSubmit={handleSubmit}>
          <p className="font-medium text-slate-700 pb-2">New Password</p>
          <input
            id="newPassword"
            type="password"
            name="newPassword"
            defaultValue=""
            className="w-full py-3 w- border border-slate-200 rounded-lg px-3 focus:outline-none focus:border-slate-500 hover:shadow"
            placeholder="Enter new password"
          />
          <br />
          <p className="font-medium text-slate-700 p-3">Confirm New Password</p>
          <input
            id="confirmNewPassword"
            type="password"
            name="confirmNewPassword"
            defaultValue=""
            className="w-full py-3 w- border border-slate-200 rounded-lg px-3 focus:outline-none focus:border-slate-500 hover:shadow"
            placeholder="Enter confirm new password"
          />
          <br />
          <article className="w-64 text-wrap">
            <p className="text text-red-400">{error?.error?.message ?? ""}</p>
          </article>
          <br />
          <button
            className="w-full py-3 font-medium text-white bg-indigo-600 hover:bg-indigo-500 rounded-lg border-indigo-500 hover:shadow inline-flex space-x-2 items-center justify-center"
            type="submit"
            disabled={loading}
          >
            {loading && (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth="1.5"
                stroke="currentColor"
                className="w-6 h-6 animate-spin"
              >
                <circle
                  cx="12"
                  cy="12"
                  r="10"
                  stroke="currentColor"
                  strokeWidth="4"
                />
              </svg>
            )}
            <span>Submit</span>
          </button>
          <br />
        </form>
      </div>
    </>
  );

  // JSX for the main component
  return (
    <>
      {loading ? (
        <Loading />
      ) : (
        <main className="grid min-h-full place-items-center bg-white px-6 py-24 sm:py-32 lg:px-8">
          <div className="max-w-96 text-center word-wrap">
            <h1 className="mt-4 text-3xl font-bold tracking-tight text-gray-900 sm:text-5xl">
              Reset Password
            </h1>

            {error?.error
              ? error.error && error.error.code === errorCodes.VALIDATION_ERROR
                ? resetFormHtml
                : errorMessageHtml
              : resetFormHtml}
          </div>
        </main>
      )}
    </>
  );
};

export default ResetPasswordPage;
