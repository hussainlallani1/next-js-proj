import React from "react";
import Link from "next/link";

const ResetPasswordSuccessPage = () => {
  return (
    <main className="grid min-h-full place-items-center bg-white px-6 py-24 sm:py-32 lg:px-8">
      <div className="max-w-96 text-center word-wrap">
        <h1 className="mt-4 text-3xl font-bold tracking-tight text-gray-900 sm:text-5xl">
          Password Reset Successful
        </h1>
        <p className="pt-5 mt-5 text-secondary text-2xl">
          Your password has been successfully updated!
        </p>
        <br />
        <br />
        <p className="text-gray-600">
          Continue to the <Link href="/api/auth/signin">Sign-in page</Link>.
        </p>
      </div>
    </main>
  );
};

export default ResetPasswordSuccessPage;
