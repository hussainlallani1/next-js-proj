"use client";
import { redirect } from "next/dist/server/api-utils";
import React, { FormEvent } from "react";
import { useRouter } from "next/navigation";
import { useFormStatus } from "react-dom";

const ForgotPasswordForm = () => {
  const route = useRouter();
  const form = useFormStatus();
  async function onSubmit(event: FormEvent<HTMLFormElement>) {
    event.preventDefault();
    const formData = new FormData(event.currentTarget);
    const email = formData.get("email");
    console.log("Handle submit: ", email);
    if (email) route.push(`/auth/forgot-password/${email}`);
  }

  return (
    <main className="grid min-h-full place-items-center bg-white px-6 py-24 sm:py-32 lg:px-8">
      <div className="text-center">
        {/* <p className="text-base font-semibold text-indigo-600">
        {emailDecode}
      </p> */}
        <h1 className="mt-4 text-3xl font-bold tracking-tight text-gray-900 sm:text-5xl">
          Reset password
        </h1>
        <p className="mt-6 text-base leading-7 text-gray-600">
          Fill up the form to reset the password.
        </p>
        <div className="mt-10 flex items-center justify-center gap-x-6">
          <form onSubmit={onSubmit}>
            <p className="font-medium text-slate-700 pb-2">Email address</p>
            <input
              id="input-email-for-email-provider"
              type="email"
              name="email"
              defaultValue=""
              className="w-full py-3 border border-slate-200 rounded-lg px-3 focus:outline-none focus:border-slate-500 hover:shadow"
              placeholder="Enter email address"
            />
            <br />
            <br />
            <button
              className="w-full py-3 font-medium text-white bg-indigo-600 hover:bg-indigo-500 rounded-lg border-indigo-500 hover:shadow inline-flex space-x-2 items-center justify-center"
              type="submit"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth="1.5"
                stroke="currentColor"
                className="w-6 h-6"
              >
                <path
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  d="M15.75 5.25a3 3 0 013 3m3 0a6 6 0 01-7.029 5.912c-.563-.097-1.159.026-1.563.43L10.5 17.25H8.25v2.25H6v2.25H2.25v-2.818c0-.597.237-1.17.659-1.591l6.499-6.499c.404-.404.527-1 .43-1.563A6 6 0 1121.75 8.25z"
                />
              </svg>

              <span>Reset password</span>
            </button>
            <p className="text-center">
              Not registered yet?{" "}
              <a
                href="/register"
                className="text-indigo-600 font-medium inline-flex space-x-1 items-center"
              >
                <span>{form?.pending ? "Loading..." : "Register Now"} </span>
                <span>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-4 w-4"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    strokeWidth="2"
                  >
                    <path
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      d="M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14"
                    />
                  </svg>
                </span>
              </a>
            </p>
            <br />
          </form>
        </div>
      </div>
    </main>
  );
};

export default ForgotPasswordForm;
