"use client";

import Loading from "@/app/components/Loading";
import { getCsrfToken } from "next-auth/react";
import { useRouter } from "next/navigation";
import React from "react";

const ForgotEmailParamsPage = async ({
  params: { email },
}: {
  params: { email: string };
}) => {
  const route = useRouter();
  const csrfToken = await getCsrfToken();
  route.push(`/auth/forgot-password/${email}/${csrfToken}`);
  return (
    <div>
      <Loading />
    </div>
  );
};

export default ForgotEmailParamsPage;
