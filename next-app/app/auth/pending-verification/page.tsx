"use client";
import React, { FormEvent, useState } from "react";
import { useSearchParams } from "next/navigation";
import schema from "./schema";
import * as errorCodes from "@/app/constants/errorCodes";

const PendingVerificationPage = async () => {
  const params = useSearchParams();
  const identifier = params.get("identifier");
  const decodedIdentifier = decodeURIComponent(identifier!);
  const validation = schema.safeParse({
    email: decodedIdentifier,
  });
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState({});

  // Handling form submission
  const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    setLoading(true);

    const formData = new FormData(event.currentTarget);
    const email = formData.get("email")?.toString();

    setError({});
    try {
      const resetPasswordResponse = await fetch("/api/auth/resend-verification", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email,
        }),
      });

      if (resetPasswordResponse.ok) console.log("Post email to resend-verification");

      //   if (!resetPasswordResponse.ok) {
      //     const errorData = await resetPasswordResponse.json();
      //     throw errorData;
      //   } else {
      //     try {
      //       const deleteTokenResponse = await fetch(
      //         "/api/auth/delete-token",
      //         {
      //           method: "POST",
      //           headers: {
      //             "Content-Type": "application/json",
      //           },
      //           body: JSON.stringify({
      //             identifier,
      //             token,
      //           }),
      //         }
      //       );
      //       if (deleteTokenResponse.ok) {
      //         // Password reset and token deletion were successful
      //         const deleteTokenResult = await deleteTokenResponse.json();
      //         console.log("deleteTokenResult: ", deleteTokenResult);

      //         // Append "/success" to the current pathname
      //         const newPathname = `${pathname}/success`;
      //         router.push(newPathname);
      //         return deleteTokenResult;
      //       } else {
      //         // Handle errors related to token deletion
      //         const deleteTokenError = await deleteTokenResponse.json();
      //         console.error("Error deleting token:", deleteTokenError);
      //         console.log("deleteTokenError: ", deleteTokenError);
      //         return deleteTokenError;
      //       }
      //     } catch (error) {
      //       console.log("Error fetching: /api/auth/delete-token ", error);
      //     }
      //   }
    } catch (error) {
      // console.error("Error during password reset:", error);
      // throw {
      //   code: error?.error?.code,
      //   message: error?.error?.message,
      // };
      console.log("Error sending post request to resend-verification");
      setError({
        error: {
          code: error?.error?.code,
          message: error?.error?.message,
        },
      });
    } finally {
      setLoading(false);
    }
  };

  const errorHtml = (
    <main className="grid min-h-full place-items-center bg-white px-6 py-24 sm:py-32 lg:px-8">
      <div className="max-w-96 text-center word-wrap">
        <h1 className="mt-4 text-3xl font-bold tracking-tight text-gray-900 sm:text-5xl">
          Verify your Email Account
        </h1>
        <p className="mt-6 text-base leading-7 text-gray-600">
          Sorry, we couldn’t get the page you’re looking for.
        </p>
        <div className="pt-5 mt-5 w-full text-secondary text-2xl">
          {errorCodes.INVALID_OR_EXPIRED_LINK_MSG}
        </div>
      </div>
    </main>
  );

  if (!validation.success) return errorHtml;

  return (
    <div>
      <main className="grid min-h-full place-items-center bg-white px-6 py-24 sm:py-32 lg:px-8">
        <div className="text-center">
          <p className="text-base font-semibold text-indigo-600">
            {decodedIdentifier}
          </p>
          <h1 className="mt-4 text-3xl font-bold tracking-tight text-gray-900 sm:text-5xl">
            Verify your Email Account
          </h1>
          <p className="mt-6 text-base leading-7 text-gray-600">
            Check your inbox or click below to resend the verification email.
          </p>
          <div className="mt-10 flex items-center justify-center gap-x-6">
            {/* <a
              href="#"
              className="rounded-md bg-indigo-600 px-3.5 py-2.5 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
            >
              Re-send verification email
            </a> */}
            {sendVerifyEmail({
              params: {
                email: decodedIdentifier,
              },
            })}
            {/* <a href="#" className="text-sm font-semibold text-gray-900">
              Contact support <span aria-hidden="true">&rarr;</span>
            </a> */}
          </div>
        </div>
      </main>
    </div>
  );

  function sendVerifyEmail({ params }: { params: { email: string } }) {
    return (
      <form onSubmit={handleSubmit}>
        <label
          className="section-header"
          htmlFor="input-email-for-email-provider"
          hidden
        >
          Email
        </label>
        <input
          id="input-email-for-email-provider"
          type="email"
          name="email"
          defaultValue={`${params.email}`}
          hidden
        />
        <button id="submitButton" type="submit">
          <span className="rounded-md bg-indigo-600 px-3.5 py-2.5 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600">
            Resend verification email
          </span>
          <span className="text-md text-blue-900 underline decoration-1"></span>
        </button>
        <br />
      </form>
    );
  }
};

export default PendingVerificationPage;
