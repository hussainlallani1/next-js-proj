"use server";
import React from "react";

interface EmailVerificationParams {
  params: {
    email?: string | undefined;
    csrfToken?: string | undefined;
    emailAction: "pending-verification" | "reset-password" | undefined;
  };
}

const PendingVerificationPage = async ({
  params: { email, csrfToken, emailAction = "pending-verification" },
}: EmailVerificationParams) => {
  const csrfTokenDecode = decodeURIComponent(csrfToken!);
  const emailDecode = decodeURIComponent(email!);
  return (
    <div>
      <main className="grid min-h-full place-items-center bg-white px-6 py-24 sm:py-32 lg:px-8">
        <div className="text-center">
          <p className="text-base font-semibold text-indigo-600">
            {emailDecode}
          </p>
          <h1 className="mt-4 text-3xl font-bold tracking-tight text-gray-900 sm:text-5xl">
            Verify your Email Account
          </h1>
          <p className="mt-6 text-base leading-7 text-gray-600">
            Sorry, we couldn’t get the page you’re looking for.
          </p>
          <div className="mt-10 flex items-center justify-center gap-x-6">
            {/* <a
              href="#"
              className="rounded-md bg-indigo-600 px-3.5 py-2.5 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
            >
              Re-send verification email
            </a> */}
            {sendVerifyEmail({
              params: {
                email: emailDecode,
                csrfToken: csrfTokenDecode,
                emailAction,
              },
            })}
            {/* <a href="#" className="text-sm font-semibold text-gray-900">
              Contact support <span aria-hidden="true">&rarr;</span>
            </a> */}
          </div>
        </div>
      </main>
    </div>
  );

  function sendVerifyEmail({
    params: { email, csrfToken, emailAction },
  }: EmailVerificationParams) {
    return (
      <form
        action={`${process.env.NEXTAUTH_URL}/api/auth/signin/email`}
        method="POST"
      >
        <input type="hidden" name="csrfToken" value={csrfToken} readOnly />
        <input type="hidden" name="callbackUrl" value="/" />
        <label
          className="section-header"
          htmlFor="input-email-for-email-provider"
          hidden
        >
          Email
        </label>
        <input
          id="input-email-for-email-provider"
          type="email"
          name="email"
          defaultValue={`${emailAction}_${email}`}
          hidden
        />
        <button id="submitButton" type="submit">
          <span className="rounded-md bg-indigo-600 px-3.5 py-2.5 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600">
            Re-send verification email
          </span>
          <span className="text-md text-blue-900 underline decoration-1"></span>
        </button>
        <br />
      </form>
    );
  }
};

export default PendingVerificationPage;
