import { z } from "zod";

const schema = z.object({
  email: z.string().email(),
  token: z.string().refine((value) => /^[0-9a-fA-F]{64}$/.test(value), {
    message: "Invalid CSRF token format",
  }),
});

export default schema;
